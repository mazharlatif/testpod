//
//  HelloWorld.swift
//  Nimble
//
//  Created by Mazhar Latif on 11/1/20.
//

import Foundation

public class HelloWorld {
  
  public init() {}
  
  public var greeting: String {
    return "Hello, World!"
  }
  
}
